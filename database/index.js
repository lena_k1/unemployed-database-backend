const typeorm = require("typeorm");
const ormconfig = require("../ormconfig");

const connection = typeorm.createConnection(ormconfig)

module.exports = {
  connection,
};
