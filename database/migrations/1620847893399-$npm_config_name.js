const { MigrationInterface, QueryRunner } = require("typeorm");

module.exports = class CreateBaseTables1615818740674 {
  async up(queryRunner) {
    await queryRunner.query(`
                CREATE TABLE "unemployed" (
                    id SERIAL,
                    firstName VARCHAR(255),
                    lastName VARCHAR(255),
                    activity VARCHAR(255),
                    passportID VARCHAR(255),
                    identificationCode VARCHAR(255)
                );
            `);
  }

  async down(queryRunner) {}
};
