const EntitySchema = require("typeorm").EntitySchema;
const Admin = require("../model/admin").admin;

module.exports = new EntitySchema({
  name: "admin",
  target: Admin,
  columns: {
    id: {
      primary: true,
      type: "int",
      generated: true,
    },
    email: {
      type: "varchar",
    },
    password: {
      type: "varchar",
    }
  },
});
