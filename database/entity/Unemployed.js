const EntitySchema = require("typeorm").EntitySchema;
const Unemployed = require("../model/unemployed").unemployed;

module.exports = new EntitySchema({
  name: "unemployed",
  target: Unemployed,
  columns: {
    id: {
      primary: true,
      type: "int",
      generated: true,
    },
    firstName: {
      type: "varchar",
    },
    lastName: {
      type: "varchar",
    },
    activity: {
      type: "varchar",
    },
    passportID: {
      type: "int"
    },
    identificationCode: {
      type: "int",
    }
  },
});
