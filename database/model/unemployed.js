class unemployed {
  constructor(firstName, lastName, activity, passportID, identificationCode, id) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.activity = activity;
    this.passportID = passportID;
    this.identificationCode = identificationCode;
  }
}

module.exports = {
  unemployed,
};
