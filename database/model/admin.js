class admin {
  constructor(email, password, id) {
    this.id = id;
    this.email = email;
    this.password = password;
  }
}

module.exports = {
  admin,
};
