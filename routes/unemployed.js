const express = require("express");
const router = express.Router();
const responseUtil = require("../utils/api-response");
const ErrorCodes = require("../constants/errors");
const AddUnemployedCase = require("../use-cases/unemployed/add");
const GetAllUnemployedCase = require("../use-cases/unemployed/getAll");
const UpdateUnemployedCase = require("../use-cases/unemployed/update");
const GetUnemployedCase = require("../use-cases/unemployed/getOne");
const DeleteUnemployedCase = require("../use-cases/unemployed/delete")

const userRouter = () => {
  router.post("/add-unemployed", async (req, res) => {
    try {
      const addCase = new AddUnemployedCase();
      const result = await addCase.execute(
        req.body.firstName,
        req.body.lastName,
        req.body.activity,
        req.body.passportID,
        req.body.identificationCode
      );

      return responseUtil.jsonResponse(res, result);
    } catch (err) {
      return responseUtil.errorResponse(res);
    }
  });

  router.put("/update-unemployed", async (req, res) => {
    try {
      const updateCase = new UpdateUnemployedCase();
      const result = await updateCase.execute(
        req.body.id,
        req.body.firstName,
        req.body.lastName,
        req.body.activity,
        req.body.passportID,
        req.body.identificationCode
      );
      if (!result)
        return responseUtil.customErrorResponse(
          res,
          "Unemployed Not Found",
          ErrorCodes.NOT_FOUND_ERROR
        );
      return responseUtil.jsonResponse(res, result);
    } catch (err) {
      return responseUtil.errorResponse(res);
    }
  });

  router.delete("/delete-unemployed/:id", async (req, res) => {
    try {
      const deleteCase = new DeleteUnemployedCase();
      const result = await deleteCase.execute(req.params.id);

      if (!result)
        return responseUtil.customErrorResponse(
          res,
          "Unemployed Not Found",
          ErrorCodes.NOT_FOUND_ERROR
        );
      return responseUtil.messageResponse(res, "Unemployed successfully deleted");
    } catch (err) {
      return responseUtil.errorResponse(res);
    }
  });

  router.get("/unemployed", async (_, res) => {
    try {
      const getAllCase = new GetAllUnemployedCase();
      const result = await getAllCase.execute();
      return responseUtil.jsonResponse(res, result);
    } catch (err) {
      return responseUtil.errorResponse(res);
    }
  });

  router.get("/get-unemployed/:id", async (req, res) => {
    try {
      const getUnemployedCase = new GetUnemployedCase();
      const result = await getUnemployedCase.execute(req.params.id);

      if (!result)
        return responseUtil.customErrorResponse(
          res,
          "Unemployed Not Found",
          ErrorCodes.NOT_FOUND_ERROR
        );

      return responseUtil.jsonResponse(res, result);
    } catch (err) {
      return responseUtil.errorResponse(res, err, 400);
    }
  });

  return router;
};

module.exports = userRouter();
