const express = require("express");
const router = express.Router();
const responseUtil = require("../utils/api-response");
const ErrorCodes = require("../constants/errors");
const Login = require("../use-cases/admin/login");

const userRouter = () => {
  router.post("/login", async (req, res) => {
    try {
      const login = new Login();
      const result = await login.execute(req.body.email, req.body.password);
      if (!result)
        return responseUtil.customErrorResponse(
          res,
          "Admin Not Found Or Invalid Password",
          ErrorCodes.NOT_FOUND_ERROR
        );
      return responseUtil.jsonResponse(res, result);
    } catch (err) {
      return responseUtil.errorResponse(res);
    }
  });

  return router;
};

module.exports = userRouter();
