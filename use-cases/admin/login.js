const typeorm = require("typeorm");
const Admin = require("../../database/entity/Admin");

module.exports = class Login {
  constructor() {
    this.repository = typeorm.getRepository(Admin);
  }
  async execute(email, password) {
    const data = await this.repository.findOne({ email, password });
    if (!data) return data;
    return {id: data.id, email: data.email};
  }
};
