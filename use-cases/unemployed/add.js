const typeorm = require("typeorm");
const Unemployed = require("../../database/entity/Unemployed");

module.exports = class AddUnemployed {
  constructor() {
    this.repository = typeorm.getRepository(Unemployed);
  }
  async execute(firstName, lastName, activity, passportID, identificationCode) {
    const newUnemployed = await this.repository.insert({
      firstName,
      lastName,
      activity,
      passportID,
      identificationCode,
    });
    const data = await this.repository.findOne({
      id: newUnemployed.identifiers[0].id,
    });
    return data;
  }
};
