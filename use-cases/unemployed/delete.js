const typeorm = require("typeorm");
const Unemployed = require("../../database/entity/Unemployed");

module.exports = class GetAllUnemployedCase {
  constructor() {
    this.repository = typeorm.getRepository(Unemployed);
  }
  async execute(id) {
    const unemployed = await this.repository.findOne({ id });
    if (!unemployed) return;
    await this.repository.delete(id);
    return unemployed;
  }
};
