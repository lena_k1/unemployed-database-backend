const typeorm = require("typeorm");
const Unemployed = require("../../database/entity/Unemployed");

module.exports = class AddUnemployed {
  constructor() {
    this.repository = typeorm.getRepository(Unemployed);
  }
  async execute(
    id,
    firstName,
    lastName,
    activity,
    passportID,
    identificationCode
  ) {
    await this.repository.update(
      { id },
      {
        firstName,
        lastName,
        activity,
        passportID,
        identificationCode,
      }
    );
    const data = await this.repository.findOne({ id });
    return data;
  }
};
