const typeorm = require("typeorm");
const Unemployed = require("../../database/entity/Unemployed");

module.exports = class GetAllUnemployedCase {
  constructor() {
    this.repository = typeorm.getRepository(Unemployed);
  }
  async execute(id) {
    const data = await this.repository.findOne({ id });
    return data;
  }
};
