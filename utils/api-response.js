const ErrorCodes = require("../constants/errors");

const apiResponseUtil = {
  jsonResponse(res, data) {
    return res.status(200).send({ data });
  },

  messageResponse(res, message) {
    return res.status(200).send({ message });
  },

  errorResponse(res) {
    return res
      .status(ErrorCodes.INTERNAL_SERVER_ERROR)
      .send({ error: "Internal Server Error" });
  },

  customErrorResponse(res, error, status) {
    return res.status(status).send(error);
  },
};

module.exports = apiResponseUtil;
