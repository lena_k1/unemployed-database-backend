require("dotenv").config();
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
const express = require("express");
const http = require("http");
const cors = require("cors");
const cookieParser = require("cookie-parser");
const unemployed = require("./routes/unemployed");
const admin = require("./routes/admin");
const database = require("./database");
const app = express();

app.use(cors());
app.use(cookieParser());
app.use(express.json());
app.use(express.raw());
app.use(express.urlencoded({ extended: false }));
app.set("port", process.env.PORT || 8080);
app.set("env", process.env.NODE_ENV);
app.disable("x-powered-by");

app.use("/api/", unemployed);
app.use("/admin/", admin);
app.use((req, res) => res.sendStatus(404));

database.connection.then(() => {
  console.log("Database is connected");
});

const server = http.createServer(app);

server.listen(app.get("port"), () => {
  console.log(`API listening on port ${app.get("port")}`);
});

server.on("error", (error) => {
  console.error(error);
});
